#ifndef _XT_HTTPHOST_H_
#define _XT_HTTPHOST_H_

#include <linux/types.h>

#define XT_HTTPHOST_MAX_HTTPHOST_SIZE 256

enum {
	XT_HTTPHOST_FLAG_INVERT		= 0x01,
};

struct xt_httphost_info {
	unsigned char	httphost[XT_HTTPHOST_MAX_HTTPHOST_SIZE];
	int				invert;
};

#endif
