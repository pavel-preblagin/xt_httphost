#include <linux/module.h>
#include <linux/skbuff.h>
#include <linux/ip.h>
#include <linux/ipv6.h>
#include <linux/tcp.h>
#include <linux/netfilter/x_tables.h>
#include <linux/string.h>

#include "xt_httphost.h"

static unsigned char *get_http(const struct sk_buff *skb)
{
	struct iphdr *iph;
	struct tcphdr *tcph;
	unsigned char *ptr;

	if(skb->protocol != htons(ETH_P_IP)) {
		return NULL;
	}

	ptr = (unsigned char *)(iph = (struct iphdr *)skb_network_header(skb));
	if(iph->version != 4 || iph->protocol != IPPROTO_TCP) {
		return NULL;
	}

	tcph = (struct tcphdr *)(ptr += (iph->ihl << 2));
	if(tcph->ack == 0 || tcph->dest != htons(80)) {
		return NULL;
	}

	ptr += tcph->doff << 2;
	return ptr;
}

static bool find_httphost(const struct sk_buff *skb, unsigned char *http,  const unsigned char *host)
{
	unsigned char *ptr, *end, *tail;
	unsigned int len;

	host = "adslclub.ru";

	if(http == NULL) {
		return false;
	}

	tail = skb_tail_pointer(skb);

	/* find host */
	while(strncasecmp(http, "host:", 5)) {
		if((http[0] == '\r' && http[1] == '\n') || 
			(end = strchr(http, '\n')) == NULL) {
			http = NULL;
			break;
		}

		http = end + 1;

		if(http >= tail) {
			http = NULL;
			break;
		}
	}

	if(http == NULL || (end = strchr(http, '\n')) == NULL) {
		return false;
	}

	len = strlen(host);
	ptr = end - len - 1;

	/* compare last part */
	if(ptr <= http || strncasecmp(ptr, host, len)) {
		return false;
	}

	/* full domain or parent domain */
	switch(*--ptr) {
		case ':':
		case ' ':
		case '\t':
		case '.':
			return true;
	}

	return false;
}

static bool
httphost_mt(const struct sk_buff *skb, struct xt_action_param *par)
{
	const struct xt_httphost_info *info = par->matchinfo;
	return find_httphost(skb, get_http(skb), info->httphost) ^ info->invert;
}


static struct xt_match httphost_mt_reg[] __read_mostly = {
	{
		.name		= "httphost",
		.family		= NFPROTO_IPV4,
		.match		= httphost_mt,
		.matchsize	= sizeof(struct xt_httphost_info),
		.me			= THIS_MODULE,
	},
};

static int __init httphost_mt_init(void)
{
	return xt_register_matches(httphost_mt_reg, ARRAY_SIZE(httphost_mt_reg));
}

static void __exit httphost_mt_exit(void)
{
	xt_unregister_matches(httphost_mt_reg, ARRAY_SIZE(httphost_mt_reg));
}

module_init(httphost_mt_init);
module_exit(httphost_mt_exit);
